<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AjudaController extends Controller
{
    public function ajuda() {
        return view('ajuda');
    }
    public function ajudasenha() {
        return view('ajudasenha');
    }

    public function ajudaicone() {
        return view('ajudaicone');
    }

    public function ajudalogin() {
        return view('ajudalogin');
    }
}
