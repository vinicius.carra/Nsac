<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;

class GraphController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function gerarGraficos() {
        
        return $data;
    }

    public function graficos() {
        $data = array();
        foreach(Auth::user()->dado->notas as $nota) {
            array_push($data, $nota->getDisciplina->abreviacao);
        }
        return view('graficos')->with('graficos', $data);
    }
}
