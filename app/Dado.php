<?php

    namespace App;

    use Illuminate\Database\Eloquent\Model;

    class Dado extends Model {
        protected $connection = "alunos";

        public function notas() {
            return $this->hasMany(Nota::class, 'aluno');
        }

        public function endereco() {
            return $this->belongsTo(Endereco::class, 'codigo');
        }

        public function matricula() {
            return $this->hasOne(Matricula::class, 'aluno');
        }

        public function atrasos() {
            return $this->hasMany(Matricula::class, 'aluno');
        }

        public function documento() {
            return $this->hasOne('App\Documento', 'aluno'); 
        }

        public function idioma() {
            return $this->hasOne('App\Idioma', 'matricula'); 
        }

        public function informacaoEspecifica() {
            return $this->hasOne('App\Idioma', 'matricula'); 
        }

        public function pai() {
            return $this->hasOne(Pai::class, 'codigo', 'pai');
        }

        public function mae() {
            return $this->hasOne(Pai::class, 'codigo', 'mae');
        }
    }
