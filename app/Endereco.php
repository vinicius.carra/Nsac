<?php

    namespace App;

    use Illuminate\Database\Eloquent\Model;

    class Endereco extends Model {
        protected $connection = "alunos";

        public function dados() {
            return $this->hasMany(Dado::class, 'endereco');
        }
    }
