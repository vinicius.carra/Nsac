@extends('layouts.app')

@section('content')
    <!-- Main img -->
    <div class="row">
        <div class="col-md-12">
            <img class="logo-main center-block" src="images/cti_icon.png" alt="Logo CTI">
        </div>
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <p class="text-main"></p>
        </div>
        <div class="col-md-4"></div>
    </div>
    <!-- Feature -->
    <br><br>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4">
                <div class="center-block">
                    <div class="container-fluid">
                        <h4 class="feature-title">Requisitos</h4>
                        <p class="feature-text">
                            <ul>
                                <li><small>ter sido matriculado no colégio</small></li>
                                <li><small>ter conhecimento do seu RA ou email cadastrado no sistema</small></li>
                                <li><small>saber a senha</small></li>
                            </ul>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="center-block">
                    <div class="container-fluid">
                        <h4 class="feature-title">Logar</h4>
                        <p class="feature-text">
                            <ul>
                                <li><small>No campo "R.A." entre com seu R.A. estudantil ou email cadastrado</small></li>
                                <li><small>No campo "Senha" entre com sua senha</small></li>
                            </ul>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="center-block">
                    <div class="container-fluid">
                        <h4 class="feature-title">Esqueci meu login ou senha</h4>
                        <p class="feature-text">
                            <ul>
                                <li><small>Caso tenha esquecido seu RA ou email cadastrado é necessário entrar em contato com a secretaria para obtenção dos dados</small></li>
                                <li><small>Caso tenha esquecido sua senha use o campo "Esqueci minha senha"</small></li>
                            </ul>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection