@extends('layouts.app')

@section('content')
    <!-- Main img -->
    <div class="row">
        <div class="col-md-12">
            <img class="logo-main center-block" src="images/cti_icon.png" alt="Logo CTI">
        </div>
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <p class="text-main"></p>
        </div>
        <div class="col-md-4"></div>
    </div>
    <!-- Feature -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4">
                <a class="feature-link" href="" target="_blank">
                    <div class="feature-card center-block">
                        <div class="container-fluid">
                            <img class="feature-img" src="images/face_icon.png" alt="">
                            <h4 class="feature-title">Ícones</h4>
                            <p class="feature-text">
                                <small>Adicione ícones a tela inicial do seu smartphone ou computador.</small>
                            </p>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-4">
                <a class="feature-link" href="" target="_blank">
                    <div class="feature-card center-block">
                        <div class="container-fluid">
                            <img class="feature-img" src="images/unesp_icon.jpg" alt="">
                            <h4 class="feature-title">Login</h4>
                            <p class="feature-text">
                                <small>Faça login no sistema NSac.</small>
                            </p>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-4">
                <a class="feature-link" href="" target="_blank">
                    <div class="feature-card center-block">
                        <div class="container-fluid">
                            <img class="feature-img not-rounded" src="images/adde_icon.png" alt="">
                            <h4 class="feature-title">Senha</h4>
                            <p class="feature-text">
                                <small>Recupere sua senha.</small>
                            </p>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
@endsection