@extends('layouts.app')

@section('content')
    <div id="mbg">    </div>
    <div class="panel-group col-sm-6">
        @foreach ($graficos as $grafico)
        <div class="panel panel-primary">
            <div class="panel-heading" data-toggle="collapse" href="#7367b">
                <h4 class="panel-title">
                    Gestão de negócios II
                </h4>
            </div>
            <div class="panel-collapse collapse" id="7367b">
                <div class="panel-body">
                {{ $grafico }}
                </div>
            </div>
        </div>
        @endforeach
    </div>
@endsection
