@extends('layouts.app')

@section('content')
<!-- Main img -->
    <div class="row">
        <div class="col-md-12">
            <img class="logo-main center-block" src="images/cti_icon.png" alt="Logo CTI">
        </div>
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <p class="text-main"></p>
        </div>
        <div class="col-md-4"></div>
    </div>
    <!-- Feature -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4">
                <a class="feature-link" href="ajudaicone" target="">
                    <div class="feature-card center-block">
                        <div class="container-fluid"/>
                            <span class="glyphicon glyphicon-phone" style="font-size: 50px"></span>
                            <h4 class="feature-title">Ícones</h4>
                            <p class="feature-text">
                                <small>Adicione ícones a tela inicial do seu smartphone ou computador.</small>
                            </p>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-4">
                <a class="feature-link" href="ajudalogin" target="">
                    <div class="feature-card center-block">
                        <div class="container-fluid">
                            <span class="glyphicon glyphicon-log-in" style="font-size: 50px"></span>
                            <h4 class="feature-title">Login</h4>
                            <p class="feature-text">
                                <small>Faça login no sistema NSac.</small>
                            </p>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-4">
                <a class="feature-link" href="ajudasenha" target="">
                    <div class="feature-card center-block">
                        <div class="container-fluid">
                            <span class="glyphicon glyphicon-lock" style="font-size: 50px"></span>
                            <h4 class="feature-title">Senha</h4>
                            <p class="feature-text">
                                <small>Recupere sua senha.</small>
                            </p>
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <a class="feature-link" href="ajudaicone" target="">
                    <div class="feature-card center-block">
                        <div class="container-fluid"/>
                            <span class="glyphicon glyphicon-phone" style="font-size: 50px"></span>
                            <h4 class="feature-title">Ícones</h4>
                            <p class="feature-text">
                                <small>Adicione ícones a tela inicial do seu smartphone ou computador.</small>
                            </p>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-4">
                <a class="feature-link" href="ajudalogin" target="">
                    <div class="feature-card center-block">
                        <div class="container-fluid">
                            <span class="glyphicon glyphicon-log-in" style="font-size: 50px"></span>
                            <h4 class="feature-title">Login</h4>
                            <p class="feature-text">
                                <small>Faça login no sistema NSac.</small>
                            </p>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-4">
                <a class="feature-link" href="ajudasenha" target="">
                    <div class="feature-card center-block">
                        <div class="container-fluid">
                            <span class="glyphicon glyphicon-save-file" style="font-size: 50px"></span>
                            <h4 class="feature-title">Manual</h4>
                            <p class="feature-text">
                                <small>Baixe o Manual do Usuário completo do sistema NSac.</small>
                            </p>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>

@endsection